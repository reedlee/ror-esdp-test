Rails.application.routes.draw do
  resources :achievements, only: [:index, :show, :new, :create]
end
