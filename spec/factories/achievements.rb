FactoryBot.define do
  factory :achievement do
    title { 'Some default title' }
    description { 'Some description' }

    trait :private do
      privacy { 1 }
    end

    factory :invalid_achievement do
      title {''}
    end
  end
end
