require 'rails_helper'

RSpec.describe Achievement, type: :model do
  context 'validation' do
    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_presence_of :privacy }
    it { is_expected.to validate_inclusion_of(:privacy).in_array([0, 1]) }
  end

  context 'privacy default value' do
    it 'should be 0 - it is public' do
      a = Achievement.create(title: 'some achievement')

      expect(a.title).to eql('some achievement')
      expect(a.description).to eql(nil)
      expect(a.privacy).to eql(0)
    end

    it 'should be 1 - it is private' do
      a = Achievement.create(title: 'some achievement', privacy: 1)

      expect(a.title).to eql('some achievement')
      expect(a.description).to eql(nil)
      expect(a.privacy).to eql(1)
    end
  end
end
