require 'rails_helper'

RSpec.describe AchievementsController, type: :controller do
  context '/index' do
    it 'render template' do
      get :index, params: {}
      expect(response).to render_template(:index)
    end
    it 'get 200 status' do
      get :index, params: {}
      expect(response.status).to eq(200)
    end
    it 'get array of achievements' do
      a = Achievement.create(title: 'some title')
      b = Achievement.create(title: 'some title2')
      get :index

      expect(assigns(:achievements)).to match_array([a, b])
    end
  end
  context '/show' do
    let(:achievement) { Achievement.create(title: 'some title') }
    it 'render template' do
      get :show, params: { id: achievement.id }
      expect(response).to render_template(:show)
    end
    it 'get 200 status' do
      get :show, params: { id: achievement.id }
      expect(response.status).to eq(200)
    end
    it 'get array of achievements' do
      get :show, params: { id: achievement.id }
      expect(assigns(:achievement)).to eq(achievement)
    end
  end
  context '/new' do
    it "renders :new template" do
      get :new
      expect(response).to render_template(:new)
    end

    it "assigns new Achievement object to @achievement" do
      get :new
      expect(assigns(:achievement)).to be_a_new(Achievement)
    end
  end
  context '/create' do
    context 'valid date' do
      # let(:valid_data) { { title: 'Some title', privacy: 1 } }
      let(:valid_data) {FactoryBot.attributes_for(:achievement, privacy: 0)}

      it 'redirects to achievements#show' do
        post :create, params: { achievement: valid_data }
        expect(response).to redirect_to(achievement_path(assigns[:achievement]))
      end

      it 'creates new achievement in database' do
        expect {
          post :create, params: { achievement: valid_data }
        }.to change(Achievement, :count).by(1)
      end
    end

    context 'invalid date' do
      # let(:invalid_data) { { title: '', privacy: 1 } }
      # let(:invalid_data) {FactoryBot.attributes_for(:achievement, :private, title: "")}
      let(:invalid_data) {FactoryBot.attributes_for(:invalid_achievement)}

      it 'renders :new template' do
        post :create, params: { achievement: invalid_data }

        expect(response).to render_template(:new)
      end

      it "doesn't create new achievement in database" do
        expect {
          post :create, params: { achievement: invalid_data }
        }.not_to change(Achievement, :count)
      end
    end
  end
end
