class Achievement < ApplicationRecord
  validates :title, presence: true
  validates :privacy, presence: true, inclusion: { in: [0, 1] }
  after_initialize :init

  def init
    self.privacy ||= 0
  end
end
