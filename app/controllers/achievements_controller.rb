class AchievementsController < ApplicationController
  def index
    @achievements = Achievement.all
  end

  def show
    @achievement = Achievement.find(params[:id])
  end

  def new
    @achievement = Achievement.new
  end

  def create
    @achievement = Achievement.new(achievement_params)

    if @achievement.save
      redirect_to achievement_url(@achievement)
    else
      render :new
    end
  end

  def achievement_params
    params.require(:achievement).permit(:title, :description, :privacy)
  end
end
